// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./Box.sol";

contract Zoo is Box {
    struct Animal {
        string name;
        string kind;
        string breed;
        uint8 legs;
        uint8 toes;
        address rider;
    }

    uint256 totalAnimals;
    mapping (uint256 => Animal) public animalsInTheZoo;

    event NewAnimalSpawned(uint256 animalId, Animal animal);

    function spawnNewAnimal(
        string memory name, 
        string memory kind, 
        string memory breed, 
        uint8 legs, 
        uint8 toes
    ) 
        external 
    {
        animalsInTheZoo[totalAnimals] = Animal({
            name: name,
            kind: kind,
            breed: breed,
            legs: legs,
            toes: toes,
            rider: msg.sender
        });

        emit NewAnimalSpawned(totalAnimals, animalsInTheZoo[totalAnimals]);

        totalAnimals++;
    }

    function version() public virtual override view returns (string memory) {
        return "v2.0";
    }
}
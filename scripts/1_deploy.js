// require('@nomiclabs/hardhat-ethers');
const hre = require('hardhat');

// const gnosisSafe = "0xfBe4Fb3537dd42ceBdA3dd4BA1a9E39e91516234";
const gnosisSafe = "0xbA0500a505D7FeA44B54101D54318376890cB97D";

async function main () {
    const Box = await hre.ethers.getContractFactory('Box');
    console.log('Deploying Box...');
    const box = await hre.upgrades.deployProxy(Box, [42], { kind: "uups"});

    await box.deployed();
    
    console.log(`Box deployed to: ${box.address}`);

    const impls = await hre.upgrades.erc1967.getImplementationAddress(box.address);
    console.log(`Implementation address: ${impls}`);

    await box.transferOwnership(gnosisSafe);
  }
  
  main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });
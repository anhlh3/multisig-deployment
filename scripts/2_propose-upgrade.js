const hre = require('hardhat');

async function main() {
  const proxyAddress = '0xD0b8498464923DFD56A955fF7c83264B666DD49F';
  // const gnosisSafe = "0xfBe4Fb3537dd42ceBdA3dd4BA1a9E39e91516234";
  const gnosisSafe = "0xbA0500a505D7FeA44B54101D54318376890cB97D";

  const ZooFactory = await hre.ethers.getContractFactory("Zoo");
  console.log("Preparing proposal...");
  const proposal = await hre.defender.proposeUpgrade(proxyAddress, ZooFactory, { multisig: gnosisSafe });
  console.log("Upgrade proposal created at:", proposal.url);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  })
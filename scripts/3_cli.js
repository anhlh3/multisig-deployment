// run this script using node cli

const yargs = require('yargs');
const hre = require('hardhat');
const ethers = hre.ethers;

async function main() {
    // console.log(process.argv.slice(2));

    const argv = yargs
        .command('lyr', 'Tells whether an year is leap year or not', {
            year: {
                description: 'the year to check for',
                alias: 'y',
                type: 'number',
            }
        })
        .option('time', {
            alias: 't',
            description: 'Tell the present Time',
            type: 'boolean',
        })
        .help()
        .alias('help', 'h')
        .argv;

    if (argv.time) {
        console.log('The current time is: ', new Date().toLocaleTimeString());
    }

    if (argv._.includes('lyr')) {
        const year = argv.year || new Date().getFullYear();
        if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
            console.log(`${year} is a Leap Year`);
        } else {
            console.log(`${year} is NOT a Leap Year`);
        }
    }

    console.log(argv);
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  })